namespace Umbralla.Input
{
    public class InputManager
    {
        private static InputActions inputActions;

        public static InputActions Input
        {
            get
            {
                if (inputActions == null)
                {
                    inputActions = new InputActions();
                    Input.Character.Enable();//TODO: Move this to general character control manager?
                }
                return inputActions;
            }
        }
    }
}