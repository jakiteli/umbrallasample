using UnityEngine;

namespace Umbralla.Helpers
{
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        public bool IsDontDestroyOnLoad = true;
        public static bool HasInstance => Instance;
        public static T Instance { private set; get; }

        protected virtual void Awake()
        {
            if (!(Instance is null))
            {
                if (Instance != this)
                {
                    Destroy(gameObject);
                }

                return;
            }

            Instance = (T)this;
            if (Instance.IsDontDestroyOnLoad)
            {
                DontDestroyOnLoad(Instance.gameObject);
            }
        }

        protected virtual void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }
    }
}