using System.Collections;
using System.Collections.Generic;
using Umbralla.Input;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Umbralla.Entity.Mage
{
    [RequireComponent(typeof(MageValues))]
    [RequireComponent(typeof(MageStatus))]
    [RequireComponent(typeof(MageMovement))]
    [RequireComponent(typeof(Animator))]
    public class MageActions : MonoBehaviour
    {
        private MageValues mageValues;
        private MageStatus mageStatus;
        private MageMovement mageMovement;
        private Animator animator;

        private int currentAttackIndex = -1;
        private bool canAttack = true;
        private List<Transform> entitiesHit = new List<Transform>();

        private void Awake()
        {
            mageValues = GetComponent<MageValues>();
            mageStatus = GetComponent<MageStatus>();
            mageMovement = GetComponent<MageMovement>();
            animator = GetComponent<Animator>();

            foreach(AttackValues attackValues in mageValues.Attacks)
            {
                if(!attackValues.IsValid())
                {
                    Debug.LogError($"{attackValues} is not valid!");
                }
            }
        }

        private void OnEnable()
        {
            InputManager.Input.Character.Attack.performed += ExecuteAttack;
            InputManager.Input.Character.Block.performed += ExecuteBlock;
            InputManager.Input.Character.Block.canceled += StopBlock;
        }
        private void OnDisable()
        {
            InputManager.Input.Character.Attack.performed -= ExecuteAttack;
            InputManager.Input.Character.Block.performed -= ExecuteBlock;
            InputManager.Input.Character.Block.canceled -= StopBlock;
        }

        private void ExecuteAttack(InputAction.CallbackContext context)
        {
            if ((mageStatus.IsDead || mageStatus.IsStaggered) || (mageStatus.IsAttacking && !canAttack))
            {
                return;
            }

            if (mageStatus.BlockInput)
            {
                StartAttack(mageValues.BlockAttack);
                return;
            }

            int nextAttack = (currentAttackIndex + 1) % mageValues.Attacks.Count;
            StartAttack(mageValues.Attacks[nextAttack]);
        }

        private void ExecuteBlock(InputAction.CallbackContext context)
        {
            mageStatus.BlockInput = true;
            if (mageStatus.IsBlocking)
            {
                mageMovement.BlockPushback();
            }
            animator.SetBool("IsBlocking", mageStatus.IsBlocking);
        }

        private void StopBlock(InputAction.CallbackContext context)
        {
            if (mageStatus.IsBlocking)
            {
                mageMovement.BlockPushforth();
            }
            mageStatus.BlockInput = false;
            animator.SetBool("IsBlocking", mageStatus.IsBlocking);
        }

        private void StartAttack(AttackValues attackValues)
        {
            StartCoroutine(ProcessAttack(attackValues));
        }

        private IEnumerator ProcessAttack(AttackValues attackValues)
        {
            SetAttackProperties(true);

            for (float t = 0; t < attackValues.AttackDuration; t += Time.fixedDeltaTime)
            {
                if (t >= attackValues.ColliderEnableTime && t < attackValues.ColliderDisableTime)
                {
                    List<Collider> targets = FindTargets(attackValues);
                    ApplyDamageTo(targets);
                }

                if (t >= attackValues.FollowUpTime)
                {
                    canAttack = true;
                }

                yield return new WaitForFixedUpdate();
            }


            SetAttackProperties(false);
        }

        private void SetAttackProperties(bool actionStart)
        {
            mageStatus.IsAttacking = actionStart;
            canAttack = !actionStart;
            animator.SetBool("IsAttacking", actionStart);

            if (actionStart)
            {
                currentAttackIndex++;
                entitiesHit.Clear();
            }
            else
            {
                currentAttackIndex = -1;
            }
        }

        private List<Collider> FindTargets(AttackValues attackValues)
        {
            List<Collider> targets = new List<Collider>();
            Collider[] collidersInRange = Physics.OverlapSphere(transform.position, attackValues.AttackRange, LayerMask.GetMask("Entity"));

            foreach(Collider collider in collidersInRange)
            {
                if (collider.transform == this.transform)
                {
                    continue;
                }
                if (entitiesHit.Contains(collider.transform))
                {
                    continue;
                }

                if (Vector3.Angle(transform.forward, collider.transform.position - transform.position) < attackValues.AttackAngle)
                {
                    targets.Add(collider);
                }
            }
            return targets;
        }

        private void ApplyDamageTo(List<Collider> targets)
        {
            foreach(Collider target in targets)
            {
                target.GetComponent<EntityHealth>().TakeDamage();
                entitiesHit.Add(target.transform);
            }
        }
    }
}