﻿using System;
using UnityEngine;

namespace Umbralla.Entity.Mage
{
    [Serializable]
    public class AttackValues
    {
        [SerializeField]
        private float attackRange;
        public float AttackRange => attackRange;
        [SerializeField]
        private float attackAngle;
        public float AttackAngle => attackAngle;

        [SerializeField]
        private float attackDuration;
        public float AttackDuration => attackDuration;
        [SerializeField]
        private float followUpTime;
        public float FollowUpTime => followUpTime;
        [SerializeField]
        private float colliderEnableTime;
        public float ColliderEnableTime => colliderEnableTime;
        [SerializeField]
        private float colliderDisableTime;
        public float ColliderDisableTime => colliderDisableTime;

        public bool IsValid()
        {
            bool isValid = true;
            isValid = isValid && colliderDisableTime > colliderEnableTime;
            isValid = isValid && followUpTime <= attackDuration;
            isValid = isValid && colliderDisableTime <= attackDuration;

            return isValid;
        }
    }
}