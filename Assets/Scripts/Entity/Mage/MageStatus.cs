using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Umbralla.Entity.Mage
{
    public class MageStatus : MonoBehaviour
    {
        public bool IsDead { get; set; }
        public bool IsStaggered { get; set; }
        public bool IsAttacking { get; set; }
        public bool IsBlocking => BlockInput && !IsDead && !IsAttacking && !IsStaggered;
        public bool CanMove => !IsDead && !IsAttacking && !IsStaggered;

        public bool BlockInput { get; set; }
    }
}