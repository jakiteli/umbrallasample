using System;
using Umbralla.Input;
using UnityEngine;

namespace Umbralla.Entity.Mage
{
    [RequireComponent(typeof(MageValues))]
    [RequireComponent(typeof(MageStatus))]
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(Animator))]
    public class MageMovement : MonoBehaviour
    {
        private CharacterController characterController;
        private MageValues mageValues;
        private MageStatus mageStatus;
        private Transform camTransform;
        private Animator animator;

        private Vector3 inputVector;
        private Vector3 currentPushBackValue;

        private void Awake()
        {
            mageValues = GetComponent<MageValues>();
            mageStatus = GetComponent<MageStatus>();
            characterController = GetComponent<CharacterController>();
            animator = GetComponent<Animator>();
        }

        private void Start()
        {
            camTransform = Camera.main.transform;
        }

        private void FixedUpdate()
        {
            GetInput();
            RotateCharacter();
            MoveCharacter();
            MovePushback();
            SetAnimatorVariables();
        }

        private void GetInput()
        {
            Vector2 inputVector2 = InputManager.Input.Character.Movement.ReadValue<Vector2>();

            inputVector = camTransform.forward * inputVector2.y + camTransform.right * inputVector2.x;
            inputVector.y = 0f;
        }

        private void RotateCharacter()
        {
            if (!mageStatus.CanMove || inputVector == Vector3.zero)
            {
                return;
            }

            Quaternion targetRotation = Quaternion.LookRotation(inputVector);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, mageValues.MaxRotationAngle * Time.fixedDeltaTime);
        }

        private void MoveCharacter()
        {
            if (!mageStatus.CanMove || inputVector == Vector3.zero)
            {
                return;
            }

            characterController.SimpleMove(inputVector * mageValues.MovementSpeed);
        }

        private void MovePushback()
        {
            //TODO: Playtest to see if I should disable movement
            characterController.SimpleMove(currentPushBackValue);
            currentPushBackValue -= currentPushBackValue * mageValues.PushbackDeceleration * Time.fixedDeltaTime;
            if(currentPushBackValue.magnitude < 0.5f)
            {
                currentPushBackValue = Vector3.zero;
            }
        }

        private void SetAnimatorVariables()
        {
            animator.SetFloat("MovementSpeed", inputVector.magnitude * mageValues.MovementSpeed);
        }

        public void BlockPushback()
        {
            currentPushBackValue = -transform.forward * mageValues.BlockPushback;
        }

        public void BlockPushforth()
        {
            //TODO: Check if I should split those vectors into separate "forces"
            currentPushBackValue = transform.forward * mageValues.BlockPushback;
        }

        public void Stagger(Vector3 pushVector, float staggerDuration)
        {
            //TODO: After health system.
            //Disable any other movement vector for stagger duration
        }
    }
}