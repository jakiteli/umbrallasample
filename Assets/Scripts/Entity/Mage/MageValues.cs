using System.Collections.Generic;
using UnityEngine;

namespace Umbralla.Entity.Mage
{
    public class MageValues : MonoBehaviour
    {
        [SerializeField]
        private float maxRotationAngle = 420f;
        public float MaxRotationAngle
        {
            get
            {
                return maxRotationAngle * movementSpeedMultiplier;
            }
        }

        [SerializeField]
        private float movementSpeed = 5.5f;
        public float MovementSpeed
        {
            get
            {
                return movementSpeed * movementSpeedMultiplier;
            }
        }

        private float movementSpeedMultiplier = 1f;

        [SerializeField]
        private float blockAngle = 120f;
        public float BlockAngle => blockAngle;

        [SerializeField]
        private float parryTime = 0.1f;
        public float ParryTime => parryTime;

        [SerializeField]
        private float blockPushback = 15f;
        public float BlockPushback => blockPushback;

        [SerializeField]
        private float pushbackDeceleration = 2.5f;
        public float PushbackDeceleration => pushbackDeceleration;

        [SerializeField]
        private List<AttackValues> attacks = new List<AttackValues>();
        public IReadOnlyList<AttackValues> Attacks => attacks;

        [SerializeField]
        private AttackValues blockAttack;
        public AttackValues BlockAttack => blockAttack;
    }
}